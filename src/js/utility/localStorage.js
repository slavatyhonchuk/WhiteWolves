export const loadState = () => {
	try {
		const serialiazedState = localStorage.getItem('state');
		if (serialiazedState === null) {
			return undefined;
		}
		return JSON.parse(serialiazedState);
	} catch (e) {
		return undefined;
	}
};

export const saveState = (state) => {
	try {
		const serialiazedState = JSON.stringify(state);
		localStorage.setItem('state', serialiazedState);
	} catch (e) {
		console.log(e);
	}
};
