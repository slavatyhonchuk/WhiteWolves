const timeConverter = (timeStamp) => {
	const a = new Date(timeStamp);
	const months = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Aug',
		'Sep',
		'Oct',
		'Nov',
		'Dec',
	];
	const year = a.getFullYear();
	const month = months[a.getMonth()];
	const date = a.getDate();
	const hour = a.getHours();
	const min = a.getMinutes();
	const sec = a.getSeconds();
	// const time = `${date} ${month} ${year} ${hour}:${min}:${sec}`;
	if (min < 10 && hour < 10) {
		return `0${hour}:0${min}`;
	} else if (min < 10) {
		return `${hour}:0${min}`;
	} else if (hour < 10) {
		return `0${hour}:${min}`;
	}
	return `${hour}:${min}`;

	// return time;
};
export default timeConverter;
