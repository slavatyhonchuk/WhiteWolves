import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import axios from 'axios';
import 'antd/dist/antd.css';

import '../style/index.css';

require('../../favicon.ico');

export default class Root extends Component {
	get content() {
		return <Router>{this.props.routes}</Router>;
	}
	render() {
		const LC_STATE = localStorage.getItem('state');
		if (LC_STATE) {
			const parsedUser = JSON.parse(LC_STATE).user;
			const bearerToken = parsedUser.userToken.access_token;
			if (bearerToken) {
				axios.defaults.headers.common.Authorization = `Bearer ${bearerToken}`;
			}
			axios.defaults.baseURL = 'http://88.99.217.199';
		}

		return <Provider store={this.props.store}>{this.content}</Provider>;
	}
}

Root.propTypes = {
	routes: PropTypes.element.isRequired,
	store: PropTypes.object.isRequired,
};
