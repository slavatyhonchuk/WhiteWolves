import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import user from './modules/user';
import posts from './modules/posts';
import messanger from './modules/messanger';
import profile from './modules/profile';

export default combineReducers({
	user,
	posts,
	messanger,
	routing,
	profile,
});
