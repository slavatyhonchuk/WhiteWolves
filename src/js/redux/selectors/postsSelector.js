import { createSelector } from 'reselect';

const postsDataSelector = (state) => {
	return state;
};
const resultSelector = createSelector(postsDataSelector, (payload) => {
	return payload.posts.fetchedPosts;
});

export const postsSelector = (state) => ({
	...resultSelector(state),
});
