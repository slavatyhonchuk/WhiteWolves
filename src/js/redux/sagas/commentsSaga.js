import { post } from 'axios';
import { fork, takeLatest, put } from 'redux-saga/effects';
import { constants } from '../modules/comments';
import { ADD_COMMENT } from '../../common/API/API_URLS';
import { actions } from '../modules/posts';

export function* createComments(action) {
	try {
		const commentData = yield post(ADD_COMMENT, action.payload)
			.then((result) => {
				return result;
			})
			.catch((err) => {
				console.log(err);
			});
		yield put(actions.addCommentToPost(commentData));
	} catch (error) {
		console.log(error);
	}
}

function* watchCreateComments() {
	yield takeLatest(constants.CREATE_COMMENT, createComments);
}

export const commentsSaga = [fork(watchCreateComments)];
