import { post, get } from 'axios';
import { fork, takeLatest, put, takeEvery } from 'redux-saga/effects';
import { constants, actions } from '../modules/messanger';
import {
	GET_MESSANGER_CONTACTS,
	GET_MESSAGES,
	MARK_MESSAGE_AS_READED,
} from '../../common/API/API_URLS';

export function* updateContacts() {
	try {
		const contactsList = yield get(GET_MESSANGER_CONTACTS)
			.then((res) => {
				return res.data;
			})
			.catch((err) => {
				console.log(err);
			});
		yield put(actions.updateContacts(contactsList));
	} catch (error) {
		console.log(error);
	}
}
export function* updateMessages(user) {
	const { recipient, userID } = user.payload;
	try {
		const messagesList = yield get(`${GET_MESSAGES}${userID}`)
			.then((res) => {
				return res.data;
			})
			.catch((err) => {
				console.log(err);
			});
		yield put(actions.updateMessages(messagesList, recipient));
	} catch (error) {
		console.log(error);
	}
}
export function* markMessagesAsRead(payload) {
	const { id } = payload.payload;
	console.log(payload);
	try {
		yield post(`${MARK_MESSAGE_AS_READED}`, { id })
			.then((res) => {
				console.log(res);
			})
			.catch((err) => {
				console.log(err);
			});
	} catch (error) {
		console.log(error);
	}
}
function* watchGetContacts() {
	yield takeLatest(constants.GET_CONTACTS, updateContacts);
}
function* watchGetMessages() {
	yield takeLatest(constants.GET_MESSAGES, updateMessages);
}
function* watchMarkMessagesAsRead() {
	yield takeEvery(constants.MARK_MESSAGE_AS_READED, markMessagesAsRead);
}
export const messangerSaga = [
	fork(watchGetContacts),
	fork(watchGetMessages),
	fork(watchMarkMessagesAsRead),
];
