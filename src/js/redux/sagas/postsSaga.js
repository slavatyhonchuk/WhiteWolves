import axios, { get, post } from 'axios';
import { put, fork, takeLatest } from 'redux-saga/effects';
import { constants, actions } from '../modules/posts';
import {
	GET_POSTS_API,
	LIKE_POST,
	ADD_POST_TO_FAVORITES,
} from '../../common/API/API_URLS';

export function* fetchPosts() {
	try {
		const data = yield get(GET_POSTS_API)
			.then((res) => {
				return res.data;
			})
			.catch((err) => {
				console.log(err);
			});
		yield put(actions.updatePosts(data));
	} catch (error) {
		console.log(error);
	}
}
export function* updateLikes(action) {
	try {
		if (action.payload.like) {
			const data = yield post(LIKE_POST, action.payload.postIdObj)
				.then(() => {
					return {
						postId: action.payload.postIdObj.postId,
						count: +1,
						liked: true,
					};
				})
				.catch((err) => {
					console.log(err);
				});
			yield put(actions.updatePostLikes(data));
		} else {
			const data = yield axios
				.delete(`${LIKE_POST}?post_id=${action.payload.postId}`)
				.then(() => {
					return {
						postId: action.payload.postId,
						count: -1,
						liked: false,
					};
				})
				.catch((err) => {
					console.log(err);
				});
			yield put(actions.updatePostLikes(data));
		}
	} catch (error) {
		console.log(error);
	}
}
export function* updateFavs(action) {
	try {
		if (action.payload.fav) {
			const data = yield post(
				ADD_POST_TO_FAVORITES,
				action.payload.postIdObj
			)
				.then(() => {
					return {
						postId: action.payload.postIdObj.postId,
						favourite: true,
					};
				})
				.catch((err) => {
					console.log(err);
				});
			yield put(actions.updatePostFavouriteStatus(data));
		} else {
			const data = yield axios
				.delete(
					`${ADD_POST_TO_FAVORITES}?post_id=${action.payload.postId}`
				)
				.then(() => {
					return {
						postId: action.payload.postId,
						favourite: false,
					};
				})
				.catch((err) => {
					console.log(err);
				});
			yield put(actions.updatePostFavouriteStatus(data));
		}
	} catch (error) {
		console.log(error);
	}
}
function* watchGetPosts() {
	yield takeLatest(constants.GET_POSTS, fetchPosts);
}
function* watchPostLikes() {
	yield takeLatest(constants.TOGGLE_POST_IS_LIKED, updateLikes);
}
function* watchPostFavs() {
	yield takeLatest(constants.TOGGLE_POST_IS_FAVOURITE, updateFavs);
}
export const postsSaga = [
	fork(watchGetPosts),
	fork(watchPostLikes),
	fork(watchPostFavs),
];
