import {
	put,
	race,
	call,
	all,
	take,
	fork,
	takeLatest,
} from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import {
	INITIALIZE_WEB_SOCKETS_CHANNEL,
	STOP_WEB_SOCKETS_CHANNEL,
	SEND_WEBSOCKET_MESSAGE,
	actions,
} from '../modules/messanger';
import { Stomp } from '../../utility/stomp';
import SockJS from '../../utility/sockjs';

function watchMessages(client, userToken, currentUserId, recipientId) {
	return eventChannel((emit) => {
		const headers = {
			Authorization: `Bearer ${userToken}`,
		};
		const subsCallback = (message) => {
			const receivedMsg = JSON.parse(message.body);
			emit({ receivedMsg });
		};
		const connectCallback = () => {
			client.subscribe(`/receive/${currentUserId}`, subsCallback);
			client.subscribe(`/receive/${recipientId}`, subsCallback);
		};
		const onError = (error) => {
			console.log(error);
		};
		client.connect(
			headers,
			connectCallback,
			onError
		);
		return () => {
			client.disconnect();
		};
	});
}

function* internalListener(socket, action) {
	while (true) {
		const { payload } = yield take(SEND_WEBSOCKET_MESSAGE);
		const { userToken, recipientId } = action.payload;
		const headers = {
			Authorization: `Bearer ${userToken}`,
		};
		const destination = `/chat/send/${recipientId}`;
		const body = JSON.stringify({
			recipient: recipientId,
			body: payload.payload,
		});
		socket.send(destination, headers, body);
	}
}

function* externalListener(socketChannel, currentUserId) {
	while (true) {
		const action = yield take(socketChannel);
		const { receivedMsg } = action;
		yield put(actions.messageReceived(receivedMsg));
		console.log(receivedMsg);
		if (!receivedMsg.read && receivedMsg.recipient === currentUserId) {
			yield put(actions.markMessageAsReaded(receivedMsg.id));
			yield put(actions.changeMessageReadTypeInStore(receivedMsg.id));
		}
	}
}
function* initializeWebSocketsChannel(action) {
	try {
		let WEBSOCKET_ACTIVE = true;
		while (WEBSOCKET_ACTIVE) {
			const {
				WS_PROT,
				userToken,
				currentUserId,
				recipientId,
			} = action.payload;
			const ws = new SockJS(WS_PROT);
			const client = Stomp.over(ws);
			client.heartbeat.outgoing = 20000; // client will send heartbeats every 20000ms
			client.heartbeat.incoming = 20000; // client will receive heartbeats every 20000ms
			const socketChannel = yield call(
				watchMessages,
				client,
				userToken,
				currentUserId,
				recipientId
			);
			const { cancel } = yield race({
				task: all([
					call(externalListener, socketChannel, currentUserId),
					call(internalListener, client, action),
				]),
				cancel: take(STOP_WEB_SOCKETS_CHANNEL),
			});
			if (cancel) {
				// to close socket and turn off listeners
				socketChannel.close();
				WEBSOCKET_ACTIVE = false;
			}
		}
	} catch (error) {
		console.log(error);
	}
}

export function* initWSChannel() {
	yield takeLatest(
		INITIALIZE_WEB_SOCKETS_CHANNEL,
		initializeWebSocketsChannel
	);
}

export const chatWebSocketSaga = [fork(initWSChannel)];
