import { all } from 'redux-saga/effects';
import { postsSaga } from './postsSaga';
import { commentsSaga } from './commentsSaga';
import { messangerSaga } from './messangerSaga';
import { chatWebSocketSaga } from './chatWebSocketSaga';

export default function* sagas() {
	yield all([
		...postsSaga,
		...commentsSaga,
		...messangerSaga,
		...chatWebSocketSaga,
	]);
}
