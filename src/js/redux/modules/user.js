import { createAction, handleActions } from 'redux-actions';

// ------------------------------------
// Actions
// ------------------------------------

export const SET_USER_TOKEN = 'user/SET_USER_TOKEN';
export const SET_USER_INFO = 'user/SET_USER_INFO';
// export const GET_USER_ACTIVITY = 'user/GET_USER_ACTIVITY';
// export const UPDATE_USER_ACTIVITY = 'user/UPDATE_USER_ACTIVITY';

export const constants = {
	SET_USER_INFO,
	SET_USER_TOKEN,
	// UPDATE_USER_ACTIVITY,
	// GET_USER_ACTIVITY,
};

// ------------------------------------
// Action Creators
// ------------------------------------

export const setUserInfo = createAction(SET_USER_INFO, (payload) => ({
	...payload,
}));
// export const getUserActivity = createAction(GET_USER_ACTIVITY, (payload) => ({
// 	payload,
// }));
// export const updateUserActivity = createAction(
// 	UPDATE_USER_ACTIVITY,
// 	(payload) => ({
// 		...payload,
// 	})
// );

export const actions = {
	setUserInfo,
	// getUserActivity,
};
// ------------------------------------
// Reducers
// ------------------------------------

const reducers = {
	[SET_USER_INFO]: (state, { payload }) => ({
		...state,
		...payload,
	}),
	// [UPDATE_USER_ACTIVITY]: (state, { payload }) => ({
	// 	...state,
	// 	userActivity: { ...payload },
	// }),
};

// ------------------------------------
// Export + Initial State
// ------------------------------------

const initialState = {
	userToken: {
		access_token: null,
		token_type: null,
		refresh_token: null,
		expires_in: null,
		scope: null,
	},
	userInfo: {
		id: null,
		firstName: null,
		lastName: null,
		nickName: null,
		email: null,
		phoneNumber: null,
		gender: null,
		birthday: null,
		password: null,
		roles: null,
		enabled: null,
		accountNonLocked: null,
		credentialsNonExpired: null,
		authorities: null,
		accountNonExpired: null,
		username: null,
	},
};


export default handleActions(reducers, initialState);
