import { createAction, handleActions } from 'redux-actions';

// ------------------------------------
// Actions
// ------------------------------------
export const CREATE_COMMENT = 'posts/CREATE_COMMENT';

export const constants = {
	CREATE_COMMENT,
};

// ------------------------------------
// Action Creators
// ------------------------------------
export const createComment = createAction(CREATE_COMMENT, (data) => ({
	...data,
}));

export const actions = {
	createComment,
};

// ------------------------------------
// Reducers
// ------------------------------------
export const reducers = {};
export const initialState = () => ({});
export default handleActions(reducers, initialState());
