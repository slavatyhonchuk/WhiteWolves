import { createAction, handleActions } from 'redux-actions';
// ------------------------------------
// Actions
// ------------------------------------

export const SET_CURRENT_PROFILE_INFO = 'profile/SET_CURRENT_PROFILE_INFO';

export const constants = {
	SET_CURRENT_PROFILE_INFO,
};
// ------------------------------------
// Action Creators
// ------------------------------------

export const setCurrentProfileInfo = createAction(
	SET_CURRENT_PROFILE_INFO,
	(payload) => ({
		...payload,
	})
);

export const actions = {
	setCurrentProfileInfo,
};

// ------------------------------------
// Reducers
// ------------------------------------
const User = (obj) => ({
	...obj,
	fullName: `${obj.firstName} ${obj.lastName} ${obj.nickName}`,
});
const reducers = {
	[SET_CURRENT_PROFILE_INFO]: (state, { payload }) => {
		return {
			...state,
			currentProfile: new User(payload),
		};
	},
};

// ------------------------------------
// Export + Initial State
// ------------------------------------

const initialState = {
	currentProfile: {},
};
Object.defineProperty(initialState.currentProfile, 'fullName', {
	enumerable: false,
	get() {
		return `${this.firstName} ${this.lastName} ${this.nickName}`;
	},
});

export default handleActions(reducers, initialState);
