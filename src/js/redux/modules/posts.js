import { createAction, handleActions } from 'redux-actions';

// ------------------------------------
// Actions
// ------------------------------------
const GET_POSTS = 'posts/GET_POSTS';
const UPDATE_POSTS = 'posts/UPDATE_POSTS';
const ADD_COMMENT_TO_POST = 'posts/ADD_COMMENT_TO_POST';
const SET_VISIBILITY_FILTER = 'posts/SET_VISIBILITY_FILTER';
const TOGGLE_POST_IS_LIKED = 'posts/TOGGLE_POST_IS_LIKED';
const TOGGLE_POST_IS_FAVOURITE = 'posts/TOGGLE_POST_IS_FAVOURITE';
const UPDATE_POST_LIKES = 'posts/UPDATE_POST_LIKES';
const UPDATE_POST_FAVOURITE_STATUS = 'posts/UPDATE_POST_FAVOURITE_STATUS';

export const VisibilityFilters = {
	SHOW_ALL: 'SHOW_ALL',
	SHOW_FAVOURITE: 'SHOW_FAVOURITE',
};

export const constants = {
	GET_POSTS,
	UPDATE_POSTS,
	ADD_COMMENT_TO_POST,
	SET_VISIBILITY_FILTER,
	TOGGLE_POST_IS_LIKED,
	TOGGLE_POST_IS_FAVOURITE,
	UPDATE_POST_LIKES,
	UPDATE_POST_FAVOURITE_STATUS,
};

// ------------------------------------
// Action Creators
// ------------------------------------
export const getPosts = createAction(GET_POSTS, () => ({}));
export const setVisibilityFilter = createAction(
	SET_VISIBILITY_FILTER,
	(filter) => filter
);
export const updatePosts = createAction(UPDATE_POSTS, (result) => {
	return {
		fetchedPosts: result,
		postsSuccesfullyFetched: true,
	};
});
export const addCommentToPost = createAction(
	ADD_COMMENT_TO_POST,
	(payload) => ({
		...payload,
	})
);
export const togglePostIsLiked = createAction(TOGGLE_POST_IS_LIKED, (data) => ({
	...data,
}));
export const updatePostLikes = createAction(UPDATE_POST_LIKES, (data) => ({
	...data,
}));
export const togglePostIsFavourites = createAction(
	TOGGLE_POST_IS_FAVOURITE,
	(data) => ({
		...data,
	})
);
export const updatePostFavouriteStatus = createAction(
	UPDATE_POST_FAVOURITE_STATUS,
	(data) => ({
		...data,
	})
);

export const actions = {
	getPosts,
	updatePosts,
	addCommentToPost,
	setVisibilityFilter,
	togglePostIsLiked,
	updatePostLikes,
	togglePostIsFavourites,
	updatePostFavouriteStatus,
};
// ------------------------------------
// Reducers
// ------------------------------------

export const reducers = {
	[UPDATE_POSTS]: (state, { payload }) => ({
		...state,
		...payload,
	}),
	[SET_VISIBILITY_FILTER]: (state, { payload }) => {
		return {
			...state,
			visibilityFilter: payload,
		};
	},
	[ADD_COMMENT_TO_POST]: (state, { payload }) => {
		const commentData = payload.data;
		return {
			...state,
			fetchedPosts: state.fetchedPosts.map((item) => {
				if (item.id === commentData.postId) {
					return {
						...item,
						comments: [commentData, ...item.comments],
					};
				}
				return item;
			}),
		};
	},
	[UPDATE_POST_LIKES]: (state, { payload }) => {
		const actionData = payload;
		return {
			...state,
			fetchedPosts: state.fetchedPosts.map((item) => {
				if (item.id === actionData.postId) {
					return {
						...item,
						likes: {
							count: item.likes.count + actionData.count,
							liked: actionData.liked,
						},
					};
				}
				return item;
			}),
		};
	},
	[UPDATE_POST_FAVOURITE_STATUS]: (state, { payload }) => {
		const actionData = payload;
		return {
			...state,
			fetchedPosts: state.fetchedPosts.map((item) => {
				if (item.id === actionData.postId) {
					return {
						...item,
						favourite: actionData.favourite,
					};
				}
				return item;
			}),
		};
	},
};

export const initialState = () => ({
	fetchedPosts: [],
	postsSuccesfullyFetched: false,
	visibilityFilter: VisibilityFilters.SHOW_ALL,
});
export default handleActions(reducers, initialState());
