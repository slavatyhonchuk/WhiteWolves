import { createAction, handleActions } from 'redux-actions';
import timeConverter from '../../utility/timeConvert';
// ------------------------------------
// Actions
// ------------------------------------

export const GET_MESSAGES = 'messager/GET_MESSAGES';
export const UPDATE_MESSAGES = 'messager/UPDATE_MESSAGES';
export const GET_CONTACTS = 'messager/GET_CONTACTS';
export const UPDATE_CONTACTS = 'messager/UPDATE_CONTACTS';
export const MARK_MESSAGE_AS_READED = 'messager/MARK_MESSAGE_AS_READED';
export const CHANGE_MESSAGE_READ_TYPE_IN_STORE =
	'messager/CHANGE_MESSAGE_READ_TYPE_IN_STORE';

// Chat WebSockets
export const INITIALIZE_WEB_SOCKETS_CHANNEL =
	'messager/chatWebSocket/INITIALIZE_WEB_SOCKETS_CHANNEL';
export const WEBSOCKET_MESSAGE_RECEIVED =
	'messager/chatWebSocket/WEBSOCKET_MESSAGE_RECEIVED';
export const STOP_WEB_SOCKETS_CHANNEL =
	'messager/chatWebSocket/STOP_WEB_SOCKETS_CHANNEL';
export const SEND_WEBSOCKET_MESSAGE =
	'messager/chatWebSocket/SEND_WEBSOCKET_MESSAGE';

export const constants = {
	GET_MESSAGES,
	UPDATE_MESSAGES,
	GET_CONTACTS,
	UPDATE_CONTACTS,
	INITIALIZE_WEB_SOCKETS_CHANNEL,
	WEBSOCKET_MESSAGE_RECEIVED,
	STOP_WEB_SOCKETS_CHANNEL,
	SEND_WEBSOCKET_MESSAGE,
	MARK_MESSAGE_AS_READED,
	CHANGE_MESSAGE_READ_TYPE_IN_STORE,
};
// ------------------------------------
// Action Creators
// ------------------------------------

export const getMessages = createAction(GET_MESSAGES, (userID, recipient) => ({
	userID,
	recipient,
}));
export const updateMessages = createAction(
	UPDATE_MESSAGES,
	(payload, recipient) => ({
		payload,
		recipient,
	})
);
export const getContacts = createAction(GET_CONTACTS);
export const updateContacts = createAction(UPDATE_CONTACTS, (payload) => ({
	...payload,
}));
export const markMessageAsReaded = createAction(
	MARK_MESSAGE_AS_READED,
	(id) => ({
		id,
	})
);
export const changeMessageReadTypeInStore = createAction(
	CHANGE_MESSAGE_READ_TYPE_IN_STORE,
	(id) => ({
		id,
	})
);

// Chat WebSockets
export const initChatWS = createAction(
	INITIALIZE_WEB_SOCKETS_CHANNEL,
	(payload) => ({
		...payload,
	})
);
export const sendMessageToChat = createAction(
	SEND_WEBSOCKET_MESSAGE,
	(payload) => ({
		payload,
	})
);
export const messageReceived = createAction(
	WEBSOCKET_MESSAGE_RECEIVED,
	(payload) => ({
		payload,
	})
);
export const closeSocket = createAction(
	STOP_WEB_SOCKETS_CHANNEL,
	(payload) => ({
		payload,
	})
);

export const actions = {
	getMessages,
	updateMessages,
	getContacts,
	updateContacts,
	initChatWS,
	sendMessageToChat,
	messageReceived,
	closeSocket,
	markMessageAsReaded,
	changeMessageReadTypeInStore,
};

// ------------------------------------
// Reducers
// ------------------------------------

const reducers = {
	[UPDATE_MESSAGES]: (state, payload) => {
		const messagesFromServer = payload.payload.payload;
		const { recipient } = payload.payload;
		const messagesArr = Object.values(messagesFromServer).map((item) => {
			const { ...newItem } = item;
			newItem.posted = timeConverter(item.posted);
			return newItem;
		});
		return {
			...state,
			currentChat: {
				recipient,
				messagesArr,
			},
		};
	},
	[UPDATE_CONTACTS]: (state, { payload }) => ({
		...state,
		contactsList: Object.values(payload),
	}),
	[WEBSOCKET_MESSAGE_RECEIVED]: (state, { payload }) => {
		const { ...newItem } = payload.payload;
		newItem.posted = timeConverter(payload.payload.posted);
		return {
			...state,
			currentChat: {
				...state.currentChat,
				messagesArr: [
					newItem,
					...state.currentChat.messagesArr.filter(
						(item) => item.id !== newItem.id
					),
				],
			},
		};
	},
	[CHANGE_MESSAGE_READ_TYPE_IN_STORE]: (state, { payload }) => {
		const { id } = payload;
		return {
			...state,
			currentChat: {
				...state.currentChat,
				messagesArr: state.currentChat.messagesArr.map((item) => {
					const newItem = item;
					if (item.id === id) newItem.read = true;
					return newItem;
				}),
			},
		};
	},
};

// ------------------------------------
// Export + Initial State
// ------------------------------------

const initialState = {
	contactsList: [],
	currentChat: {
		messagesArr: [],
		recipient: {},
	},
};

// console.log(initialState)
export default handleActions(reducers, initialState);
