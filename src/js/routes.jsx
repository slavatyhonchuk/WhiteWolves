import React, { Suspense, lazy } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Preloader from './common/components/Blocks/Preloader/Preloader';
import { MainMenu } from './common/components/MainMenu';

const RegistrationView = lazy(() => import('./views/registration'));
const LoginView = lazy(() => import('./views/login'));
const ChangePass = lazy(() =>
	import('./common/components/Account/ChangePass/ChangePass')
);
const RequestChangePass = lazy(() =>
	import('./common/components/Account/RequestChangePass/RequestChangePass')
);
const RegistrationConfirm = lazy(() =>
	import('./common/components/Auth/RegistrationConfirm/RegistrationConfirm')
);
const Edit = lazy(() => import('./common/components/Account/Edit/Edit'));
const LoadAvatar = lazy(() =>
	import('./common/components/Account/LoadAvatar/LoadAvatar')
);
const CreatePost = lazy(() =>
	import('./common/components/News/CreatePost/container/CreatePost')
);
const PostFeed = lazy(() =>
	import('./common/components/News/PostsFeed/container/PostFeed')
);
const Messenger = lazy(() =>
	import('./common/components/Messenger/container/Messenger')
);
const Profile = lazy(() =>
	import('./common/components/User/Profile/container/Profile')
);
const Followers = lazy(() =>
	import('./common/components/User/Followers/Followers')
);
const Following = lazy(() =>
	import('./common/components/User/Following/Following')
);

const MainMenuWithRouter = withRouter((props) => <MainMenu {...props} />);

module.exports = (
	<div className="container">
		<MainMenuWithRouter />
		<div className="container__content">
			<Suspense fallback={<Preloader />}>
				<Switch>
					<Route exact path="/user/:userId" component={Profile} />
					<Route
						exact
						path="/user/:userId/followers"
						component={Followers}
					/>
					<Route
						exact
						path="/user/:userId/following"
						component={Following}
					/>
					<Route path="/auth/login" component={LoginView} />
					<Route
						path="/auth/registration"
						component={RegistrationView}
					/>
					<Route
						path="/registrationConfirm"
						component={RegistrationConfirm}
					/>
					<Route path="/passwordChange" component={ChangePass} />
					<Route
						path="/account/requestChangePassword"
						component={RequestChangePass}
					/>
					<Route path="/account/edit" component={Edit} />
					<Route path="/account/load-avatar" component={LoadAvatar} />
					<Route path="/feed/create-post" component={CreatePost} />
					<Route exact path="/feed" component={PostFeed} />
					<Route path="/messenger" component={Messenger} />
				</Switch>
			</Suspense>
		</div>
	</div>
);
