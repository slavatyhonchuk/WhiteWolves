// GET
export const GET_LIKES = '/news-service/comment?post_id=';
export const GET_POSTS_API = '/news-service/post/all';
export const GET_CURRENT_PROFILE = '/profile-service/profile';
export const GET_PROFILE = '/profile-service/profile';
export const GET_PROFILE_BY_USER_ID = '/profile-service/profile?user_id=';
export const GET_MESSAGES =
	'/message-service/messages/history?interlocutor_id=';
export const GET_MESSANGER_CONTACTS = '/profile-service/profile/all';

// POST
export const LIKE_POST = '/news-service/like';
export const ADD_POST_TO_FAVORITES = '/news-service/favourite';
export const ADD_COMMENT = '/news-service/comment';
export const UPLOAD_AVATAR = '/picture-service/avatar';
export const MARK_MESSAGE_AS_READED = '/message-service/messages/read';

// DELETE
export const DELETE_AVATAR = '/picture-service/avatar';

//
// Subscribe
const GET_ALL_YOU_SUBSCRIBED = '/profile-service/subscribe/user';
const GET_ALL_YOUR_FOLLOWERS = '/profile-service/subscribe/followers';
const SUBSCRIBE_TO_USER = '/profile-service/subscribe';
const UNSUBSCRIBE = '/profile-service/subscribe?subscribed_user_id=';
export const subscribes = {
	GET_ALL_YOUR_FOLLOWERS,
	GET_ALL_YOU_SUBSCRIBED,
	SUBSCRIBE_TO_USER,
	UNSUBSCRIBE,
};
// Activities
const GET_USER_POSTS = '/news-service/post/user';
const GET_USER_COMMENTS = '/news-service/comment/user';
const GET_USER_LIKES = '/news-service/like/user';
export const activities = {
	GET_USER_POSTS,
	GET_USER_COMMENTS,
	GET_USER_LIKES,
};
