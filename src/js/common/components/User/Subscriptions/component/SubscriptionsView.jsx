import React from 'react';
import { Link } from 'react-router-dom';

const SubscriptionsView = (props) => (
	<div className="UserInfoView_buttons-and-links">
		<Link to={`/user/${props.userId}/followers`}>
			<div className="item-list__item btn">
				<span className="item-list__item-left ">Followers</span>
				<span className="item-list__item-right text_accent">
					{props.followersQuantity}
				</span>
			</div>
		</Link>
		<Link to={`/user/${props.userId}/following`}>
			<div className="item-list__item btn">
				<span className="item-list__item-left">Following</span>
				<span className="item-list__item-right text_accent">
					{props.followingQuantity}
				</span>
			</div>
		</Link>
		<div className="btn" onClick={props.follow}>
			Follow
		</div>
	</div>
);
export default SubscriptionsView;
