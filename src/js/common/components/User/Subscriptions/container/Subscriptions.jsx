import React, { Component } from 'react';
import axios, { get, post } from 'axios';
import { subscribes } from '../../../../API/API_URLS';
import SubscriptionsView from '../component/SubscriptionsView';

export default class Subscriptions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			followersQuantity: null,
			followingQuantity: null,
			followed: false,
		};
		this.getSubscriptions();
	}
	getSubscriptions = () => {
		get(subscribes.GET_ALL_YOUR_FOLLOWERS)
			.then((res) => {
				this.setState({
					followersQuantity: res.data.length,
				});
			})
			.catch((err) => {
				console.log(err);
			});
		get(subscribes.GET_ALL_YOU_SUBSCRIBED)
			.then((res) => {
				this.setState({
					followingQuantity: res.data.length,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	follow = () => {
		const data = {
			toFollowUserId: this.props.userId,
		};
		if (this.state.followed) {
			axios
				.delete(subscribes.UNSUBSCRIBE + this.props.userId)
				.then((res) => {
					console.log(res);
					this.setState({
						followed: false,
						followersQuantity: this.state.followersQuantity - 1,
					});
				})
				.catch((err) => {
					console.log(err);
				});
		} else {
			post(subscribes.SUBSCRIBE_TO_USER, data)
				.then((res) => {
					console.log(res);
					this.setState({
						followed: true,
						followersQuantity: this.state.followersQuantity + 1,
					});
				})
				.catch((err) => {
					console.log(err);
				});
		}
	};

	render() {
		return (
			<SubscriptionsView
				userId={this.props.userId}
				{...this.state}
				follow={this.follow}
			/>
		);
	}
}
