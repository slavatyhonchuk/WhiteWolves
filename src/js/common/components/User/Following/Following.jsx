import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get } from 'axios';
import { subscribes } from '../../../API/API_URLS';
import UsersList from '../../Blocks/UsersList/UsersList';

const mstp = (state) => ({
	id: state.profile.currentProfile.userId,
});
@connect(mstp)
class Following extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		get(subscribes.GET_ALL_YOU_SUBSCRIBED)
			.then((res) => {
				this.setState({
					...res.data,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	render() {
		return <UsersList />;
	}
}

export default Following;
