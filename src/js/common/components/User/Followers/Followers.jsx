import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import { get } from 'axios';
import { subscribes } from '../../../API/API_URLS';
import Preloader from '../../Blocks/Preloader/Preloader';

const UsersList = React.lazy(() => import('../../Blocks/UsersList/UsersList'));

const mapStateToProps = (state) => ({
	id: state.profile.currentProfile.userId,
	currentProfile: state.profile.currentProfile,
});
@connect(mapStateToProps)
class Followers extends Component {
	constructor(props) {
		super(props);
		get(subscribes.GET_ALL_YOUR_FOLLOWERS)
			.then((res) => {
				this.setState({
					data: Object.values(res.data),
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}
	render() {
		return (
			<Suspense fallback={<Preloader />}>
				<UsersList data={this.state.data} />
			</Suspense>
		);
	}
}

export default Followers;
