import React, { Component } from 'react';
import { get } from 'axios';
import { connect } from 'react-redux';
import { GET_PROFILE_BY_USER_ID } from '../../../../API/API_URLS';
import ProfileView from '../component/ProfileView';
import AllActivityTabs from '../../Activity/AllActivityTabs/container/AllActivityTabs';
import UserInfo from '../../UserInfo/container/UserInfo';
import Subscriptions from '../../Subscriptions/container/Subscriptions';
import Preloader from '../../../Blocks/Preloader/Preloader';
import { actions } from '../../../../../redux/modules/profile';

const mDTP = {
	...actions,
};
@connect(
	null,
	mDTP
)
export default class Profile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
		};
		this.getUser();
	}
	getUser = () => {
		const { pathname } = this.props.location;
		this.token = pathname.slice(6);
		get(GET_PROFILE_BY_USER_ID + this.token)
			.then((res) => {
				this.setState({
					...res.data,
					loading: false,
				});
				this.props.setCurrentProfileInfo(res.data);
			})
			.catch((err) => {
				console.log(err);
			});
	};

	render() {
		return this.state.loading ? (
			<Preloader size={55} />
		) : (
			<ProfileView>
				<UserInfo {...this.state}>
					<Subscriptions userId={this.token} />
				</UserInfo>
				<AllActivityTabs userId={this.token} />
			</ProfileView>
		);
	}
}
