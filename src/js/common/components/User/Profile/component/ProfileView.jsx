import React from 'react';

const ProfileView = (props) => (
	<div>
		{props.children}
	</div>
);

export default ProfileView;
