import React from 'react';
import { Link } from 'react-router-dom';

import '../user-info.css';

const UserInfoView = (props) => (
	<section className="UserInfoView">
		<div className="UserInfoView_first content-card">
			<div className="UserInfoView__left">
				<div className="UserInfoView__avatar-holder">
					<img
						src={props.avatar}
						alt="User avatar"
						title="User avatar"
						className="UserInfoView__avatar"
					/>
				</div>
				<div className="UserInfoView__rating text_small">
					<span>Rating</span>
					<span className="text_accent">{props.rating}</span>
				</div>
			</div>
			<div className="UserInfoView__right">
				<div className="UserInfoView__info-holder item-list text_small">
					<div className="item-list__item">
						<span className="item-list__item-right text_bold">
							{`${props.firstName} ${props.lastName} ${
								props.nickName
							}`}
						</span>
					</div>
					{props.interests ? (
						<div className="item-list__item">
							<span className="item-list__item-left">Group</span>
							<span className="item-list__item-right text_accent">
								{props.interests}
							</span>
						</div>
					) : null}
					<div className="item-list__item">
						<span className="item-list__item-left">Location</span>
						<span className="item-list__item-right text_accent">
							{`${props.city}, ${props.country}`}
						</span>
					</div>
					<div className="item-list__item">
						<span className="item-list__item-left">Gender</span>
						<span className="item-list__item-right text_accent">
							{props.gender}
						</span>
					</div>
					<div className="item-list__item">
						<span className="item-list__item-left">Birthday</span>
						<span className="item-list__item-right text_accent">
							{props.birthday}
						</span>
					</div>
					{props.children}
				</div>
				<Link className="btn" to="/account/edit">
					Edit
				</Link>
			</div>
		</div>
		<div className="UserInfoView_second content-card">
			<div className="UserInfoView_about">
				<header className="text_bold">About</header>
				<article className="text_small">{props.about}</article>
			</div>
		</div>
	</section>
);

export default UserInfoView;
