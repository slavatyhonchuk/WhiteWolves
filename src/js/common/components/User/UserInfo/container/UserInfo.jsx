import React, { PureComponent } from 'react';
import UserInfoView from '../component/UserInfoView';

export default class UserInfo extends PureComponent {
	render() {
		return <UserInfoView {...this.props} />;
	}
}
