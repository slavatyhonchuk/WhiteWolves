import React from 'react';
import './ActivityItemLike.css';

const ActivityItemLike = (props) => (
	<div className="UserActivity-like UserActivity-item">
		<header className="UserActivity-like__header UserActivity-item__header">
			Liked:
		</header>
		<div className="UserActivity-like__content UserActivity-item__content">
			<div className="UserActivity-post__content UserActivity-item__content">
				<div className="UserActivity-post__img UserActivity-item__img">
					<img src={props.postPhotoLink} alt="" />
				</div>
				<div className="UserActivity-post__heading UserActivity-item__heading">
					{props.postHeading}
				</div>
			</div>
		</div>
	</div>
);
export default ActivityItemLike;
