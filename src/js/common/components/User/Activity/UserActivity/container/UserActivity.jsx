import React, { Component } from 'react';
import axios from 'axios';
import { activities } from '../../../../../API/API_URLS';
import UserActivityView from '../component/UserActivityView';
import ActivityItemPost from '../../ActivityItemPost/ActivityItemPost';
import ActivityItemLike from '../../ActivityItemLike/ActivityItemLike';
import ActivityItemComment from '../../ActivityItemComment/ActivityItemComment';

export default class UserActivity extends Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.getActivity();
	}
	getActivity = () => {
		this.getActivityLikes();
		this.getActivityComments();
		this.getActivityPosts();
	};
	getActivityLikes = () => {
		axios({
			method: 'get',
			url: activities.GET_USER_LIKES,
			headers: {},
		})
			.then((result) => {
				this.setState({
					likes: result.data,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	getActivityComments = () => {
		axios({
			method: 'get',
			url: activities.GET_USER_COMMENTS,
			headers: {},
		})
			.then((result) => {
				this.setState({
					comments: result.data,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	getActivityPosts = () => {
		axios({
			method: 'get',
			url: activities.GET_USER_POSTS,
			headers: {},
		})
			.then((result) => {
				this.setState({
					posts: result.data,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	render() {
		return this.state.posts && this.state.likes && this.state.comments ? (
			<UserActivityView>
				{this.state.posts.map((item) => (
					<ActivityItemPost {...item} key={item.createTimestamp} />
				))}
				{this.state.likes.map((item) => (
					<ActivityItemLike {...item} key={item.createTimestamp} />
				))}
				{this.state.comments.map((item) => (
					<ActivityItemComment {...item} key={item.createTimestamp} />
				))}
			</UserActivityView>
		) : null;
	}
}
