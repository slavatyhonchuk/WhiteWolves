import React from 'react';
import '../UserActivity.css';

const UserActivityView = (props) => (
	<div className="UserActivity content-card">{props.children}</div>
);

export default UserActivityView;
