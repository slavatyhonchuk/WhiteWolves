import React from 'react';
import './ActivityItemComment.css';

const ActivityItemComment = (props) => (
	<div className="UserActivity-comment UserActivity-item">
		<header className="UserActivity-comment__header UserActivity-item__header">
			Commented:
		</header>
		<div className="UserActivity-comment__content UserActivity-item__content">
			<div className="UserActivity-comment__heading UserActivity-item__heading">
				{`"${props.comment}"`}
			</div>
		</div>
		<div>on post:</div>
		<div className="UserActivity-post__content UserActivity-item__content">
			<div className="UserActivity-post__img UserActivity-item__img">
				<img src={props.postPhotoLink} alt="" />
			</div>
			<div className="UserActivity-post__heading UserActivity-item__heading">
				{props.postHeading}
			</div>
		</div>
	</div>
);
export default ActivityItemComment;
