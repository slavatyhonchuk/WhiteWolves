import React from 'react';
import '../AllActivityTabs.css';

const AllActivityTabsView = (props) => (
	<section className="AllActivityTabsView">
		<header className="AllActivityTabsView__header">
			<nav className="navigation PostFeedView_header content-card">
				{props.tabs.map((item) => (
					<div
						key={item}
						onClick={() => props.changeTab(item)}
						className={
							props.activeTab === item ? 'btn btn_active' : 'btn'
						}
					>
						{item}
					</div>
				))}
			</nav>
		</header>
		<div>{props.children}</div>
	</section>
);

export default AllActivityTabsView;
