import React, { Component } from 'react';
import AllActivityTabsView from '../component/AllActivityTabsView';
import PostsFeed from '../../../../News/PostsFeed/container/PostFeed';
import UserActivity from '../../UserActivity/container/UserActivity';

class AllActivityTabs extends Component {
	constructor(props) {
		super(props);
		this.POSTS = 'Posts';
		this.ACTIVITY = 'Activity';
		this.state = {
			activeTab: this.POSTS,
			tabs: [this.POSTS, this.ACTIVITY],
		};
	}
	changeTab = (tab) => {
		this.setState({
			activeTab: tab,
		});
	};
	render() {
		return (
			<AllActivityTabsView
				activeTab={this.state.activeTab}
				tabs={this.state.tabs}
				changeTab={this.changeTab}
			>
				{this.state.activeTab === this.POSTS ? (
					<PostsFeed />
				) : (
					<UserActivity userId={this.props.userId} />
				)}
			</AllActivityTabsView>
		);
	}
}

export default AllActivityTabs;
