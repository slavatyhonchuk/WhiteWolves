import React from 'react';
import './ActivityItemPost.css';

const ActivityItemPost = (props) => (
	<div className="UserActivity-post UserActivity-item">
		<header className="UserActivity-post__header UserActivity-item__header">
			Created post:
		</header>
		<div className="UserActivity-post__content UserActivity-item__content">
			<div className="UserActivity-post__img UserActivity-item__img">
				<img src={props.photoLink} alt="" />
			</div>
			<div className="UserActivity-post__heading UserActivity-item__heading">
				{props.heading}
			</div>
		</div>
	</div>
);
export default ActivityItemPost;
