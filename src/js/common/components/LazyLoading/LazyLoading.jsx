import React, { PureComponent } from 'react';

import './LazyLoading.css';

class LazyLoading extends PureComponent {
	getMessage() {
		const { isLoading, timedOut, pastDelay, error, retry } = this.props;
		const errorMessage =
			'We can&apos;t pull up information at this point, please try again.';
		if (isLoading) {
			if (timedOut) {
				return <div>{errorMessage}</div>;
			} else if (pastDelay) {
				return <div className="loader">Loading...</div>;
			}
			return null;
		} else if (error) {
			return (
				<div>
					{errorMessage}
					<button className="btn btn_submit" onClick={retry}>
						Retry
					</button>
				</div>
			);
		}

		return null;
	}

	render() {
		return this.getMessage();
	}
}
// const LazyLoading = (props) => {
// 	console.log(props);
// 	if (props.error) {
// 		return (
// 			<div>
// 				Error! <button onClick={props.retry}>Retry</button>
// 			</div>
// 		);
// 	} else if (props.timedOut) {
// 		return (
// 			<div>
// 				Taking a long time...{' '}
// 				<button onClick={props.retry}>Retry</button>
// 			</div>
// 		);
// 	} else if (props.pastDelay) {
// 		return <div>Loading...</div>;
// 	} else {
// 		return null;
// 	}
// };
export default LazyLoading;
