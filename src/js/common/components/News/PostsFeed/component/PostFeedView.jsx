import React from 'react';
import Button from '../../../Blocks/Button/Button';

import '../posts-feed.css';

const PostFeedView = (props) => (
	<div className="PostFeedView">
		<header className="PostFeedView_header content-card">
			<Button linkTo="/feed/create-post" text="Create Post" />
			<Button
				action={props.filterPosts}
				text={props.isFavourites ? 'All Posts' : 'Favorite Posts'}
			/>
		</header>
		<section className="PostFeedView_posts">{props.children}</section>
	</div>
);
export default PostFeedView;
