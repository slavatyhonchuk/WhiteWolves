import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import LazyLoading from '../../../LazyLoading/LazyLoading';
import Post from '../../Post/container/Post';
import {
	actions as postActions,
	VisibilityFilters,
} from '../../../../../redux/modules/posts';
import { postsSelector } from '../../../../../redux/selectors/postsSelector';
import { youtubeParser } from '../../../../../utility/youtubeParser';

const LoadPosts = Loadable({
	loader: () => import('../component/PostFeedView'),
	loading: LazyLoading,
	timeout: 10000,
	delay: 300,
});

const getVisiblePosts = (posts, filter) => {
	const postsArr = Object.values(posts);
	switch (filter) {
		case VisibilityFilters.SHOW_ALL:
			return postsArr;
		case VisibilityFilters.SHOW_FAVOURITE:
			return postsArr.filter((t) => t.favourite);
		default:
			return postsArr;
	}
};

const mapStateToProps = (state) => ({
	posts: getVisiblePosts(postsSelector(state), state.posts.visibilityFilter),
	postsSuccesfullyFetched: state.posts.postsSuccesfullyFetched,
	visibilityFilter: state.posts.visibilityFilter,
});

const mapDispatchToProps = {
	...postActions,
};

@connect(
	mapStateToProps,
	mapDispatchToProps
)
export default class PostFeed extends Component {
	constructor(props) {
		super(props);
		this.props.getPosts();
	}

	filterPosts = () => {
		if (this.props.visibilityFilter === VisibilityFilters.SHOW_FAVOURITE) {
			this.props.setVisibilityFilter(VisibilityFilters.SHOW_ALL);
		} else {
			this.props.setVisibilityFilter(VisibilityFilters.SHOW_FAVOURITE);
		}
	};

	render() {
		const Posts = this.props.posts.map((item) => {
			let videoLinkID;
			if (item.videoLink) {
				videoLinkID = youtubeParser(item.videoLink);
			}
			return (
				<Post
					key={`${item.id}${item.creatorId}${item.creationTimestamp}${
						this.props.comments
					}`}
					id={item.id}
					creationTimestamp={item.creationTimestamp}
					heading={item.heading}
					body={item.body}
					videoLink={videoLinkID}
					photoLink={item.photoLink}
					metadata={item.metadata}
					creatorId={item.creatorId}
					likes={item.likes}
					comments={item.comments}
					isInFavorites={item.favourite}
				/>
			);
		});
		return (
			<Fragment>
				{this.props.postsSuccesfullyFetched ? (
					<div className="PostFeed">
						<LoadPosts
							isFavourites={
								this.props.visibilityFilter ===
								VisibilityFilters.SHOW_FAVOURITE
							}
							filterPosts={this.filterPosts}
						>
							{Posts}
						</LoadPosts>
					</div>
				) : null}
			</Fragment>
		);
	}
}
