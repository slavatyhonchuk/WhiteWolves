import React from 'react';
import '../comments.css';

export const CommentsView = (props) => (
	<div className="CommentsView">
		<div className="CommentsView__heading">Comments:</div>
		<div className="CommentsView__content">{props.children}</div>
	</div>
);
