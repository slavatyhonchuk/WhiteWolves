import React, { Component } from 'react';
import { CommentsView } from '../component/CommentsView';
import Comment from '../../Comment/container/Comment';
import CreateComment from '../../CreateComment/container/CreateComment';

export default class Comments extends Component {
	render() {
		return (
			<CommentsView>
				{this.props.comments.map((item) => (
					<Comment
						key={`${item.createTimestamp}${item.userId}`}
						creator={item.userId}
						body={item.comment}
					/>
				))}
				<CreateComment
					postID={this.props.postID}
					onCommentCreation={this.createComment}
				/>
			</CommentsView>
		);
	}
}
