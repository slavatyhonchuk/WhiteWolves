import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PostView } from '../component/PostView';
import { actions as favActions } from '../../../../../redux/modules/posts';

const mapDispatchToProps = {
	...favActions,
};
@connect(
	null,
	mapDispatchToProps
)
export default class Post extends Component {
	toggleFavourites = () => {
		const postIdObj = { postId: this.props.id };
		const postId = this.props.id;
		if (!this.props.isInFavorites) {
			const fav = true;
			this.props.togglePostIsFavourites({ postIdObj, fav });
		} else {
			const fav = false;
			this.props.togglePostIsFavourites({ postId, fav });
		}
	};
	render() {
		return (
			<PostView
				heading={this.props.heading}
				content={this.props.body}
				videoLink={this.props.videoLink}
				id={this.props.id}
				likes={this.props.likes}
				comments={this.props.comments}
				isInFavorites={this.props.isInFavorites}
				addToFavorites={this.toggleFavourites}
				photoLink={this.props.photoLink}
				creatorId={this.props.creatorId}
			/>
		);
	}
}
