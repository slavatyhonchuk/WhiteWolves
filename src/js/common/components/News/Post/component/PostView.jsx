import React from 'react';
import '../post.css';
import { Likes } from '../../../Blocks/Likes/container/Likes';
import Comments from '../../Comments/container/Comments';

export const PostView = (props) => (
	<section className="PostView content-card">
		<header>
			<h4>{props.heading}</h4>
		</header>
		<article className="PostView__content">
			<span className="PostView__text"> {props.content}</span>
		</article>
		{props.videoLink ? (
			<div className="PostView__video">
				<iframe
					title="video"
					width="560"
					height="315"
					src={`https://www.youtube-nocookie.com/embed/${
						props.videoLink
					}?rel=0&amp;controls=0&amp;showinfo=0`}
					frameBorder="0"
					allow="autoplay; encrypted-media"
					allowFullScreen
				/>
			</div>
		) : null}
		{props.photoLink ? (
			<div className="PostView__image-wrap">
				<img className="PostView__image" src={props.photoLink} alt="" />
			</div>
		) : null}
		<div className="PostView__fav-container">
			<div className="PostView__fav" onClick={props.addToFavorites}>
				{props.isInFavorites ? 'In Favorites' : 'Add To Favorites'}
			</div>

			<Likes parentID={props.id} likes={props.likes} />
		</div>
		<Comments postID={props.id} comments={props.comments} />
	</section>
);
