import React, { Component } from 'react';
import { get } from 'axios';
import { CommentView } from '../component/CommentView';
import { GET_PROFILE_BY_USER_ID } from '../../../../API/API_URLS';

export default class Comment extends Component {
	constructor(props) {
		super(props);
		this.state = {
			avatar:
				'https://s3.amazonaws.com/cdn.33voices.com/defaults/avatar.png',
			creator: 'User Name',
		};
		this.parseUser();
	}
	parseUser = () => {
		const userId = this.props.creator;
		get(GET_PROFILE_BY_USER_ID + userId)
			.then((res) => {
				this.setState({
					avatar: res.data.avatar,
					creator: `${res.data.firstName} ${res.data.lastName}`,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	render() {
		return (
			<CommentView
				avatar={this.state.avatar}
				creator={this.state.creator}
				body={this.props.body}
				userId={this.props.creator}
			/>
		);
	}
}
