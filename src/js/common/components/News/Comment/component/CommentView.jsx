import React from 'react';
import { Link } from 'react-router-dom';

import '../comment.css';

export const CommentView = (props) => (
	<div className="CommentView">
		<Link className="CommentView__avatar" to="/user">
			<img src={props.avatar} alt={props.creator} />
		</Link>
		<div className="CommentView__content">
			<Link className="CommentView__creator" to={`/user/${props.userId}`}>
				{props.creator}
			</Link>
			<div className="CommentView__body">{props.body}</div>
		</div>
	</div>
);
