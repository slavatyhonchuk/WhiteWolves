import React from 'react';

export const CreatePostView = (props) => (
	<div className="CreatePostView">
		<div className="CreatePostView__Form">{props.children}</div>
	</div>
);
