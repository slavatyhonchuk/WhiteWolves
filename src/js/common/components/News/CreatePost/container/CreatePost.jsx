import React, { Component } from 'react';
import { post } from 'axios';
import { message } from 'antd';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import 'antd/dist/antd.css';
import { CreatePostView } from '../../CreatePost/component/CreatePostView';
import Form from '../../../Blocks/Form/Form';
import Input from '../../../Blocks/Input/Input';
import Textarea from '../../../Blocks/Textarea/Textarea';

@connect(
	null,
	{ push }
)
export default class CreatePost extends Component {
	submit = (formDATA) => {
		const CREATE_POST_URL = '/news-service/post';
		if (formDATA.heading && formDATA.body) {
			const postFormData = {
				userId: '',
				...formDATA,
			};
			post(CREATE_POST_URL, postFormData)
				.then((result) => {
					console.log(result);
					message.success('Post created!');
					this.props.push('/feed');
				})
				.catch((err) => {
					console.log(err);
					message.error('Error! Try again.');
				});
		} else {
			alert('fill heading & body');
		}
	};
	render() {
		console.log(this.props);

		return (
			<CreatePostView>
				<Form submitAction={this.submit} submitBtnText="Submit Post">
					<Input
						key="heading"
						heading="Heading"
						type="text"
						appearing="input-block_gray-bg"
						placeholder=""
						name="heading"
					/>
					<Textarea
						key="body"
						heading="Post Content"
						type="text"
						appearing="input-block_gray-bg"
						placeholder=""
						name="body"
					/>
					<Input
						key="videoLink"
						heading="Video Link (YouTube)"
						type="url"
						appearing="input-block_gray-bg"
						placeholder=""
						name="videoLink"
					/>
					<Input
						key="photoLink"
						heading="Image Link"
						type="url"
						appearing="input-block_gray-bg"
						placeholder=""
						name="photoLink"
					/>
				</Form>
			</CreatePostView>
		);
	}
}
