import React from 'react';

import '../create-comment.css';

export const CreateCommentView = (props) => (
	<div className="CreateCommentView">
		<div className="CreateCommentView__Form">{props.children}</div>
	</div>
);
