import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CreateCommentView } from './../component/CreateCommentView';
import Form from '../../../Blocks/Form/Form';
import Textarea from '../../../Blocks/Textarea/Textarea';
import { actions as commentActions } from '../../../../../redux/modules/comments';

const mapDispatchToProps = {
	...commentActions,
};
@connect(
	null,
	mapDispatchToProps
)
export default class CreatePost extends Component {
	submit = (formDATA) => {
		if (formDATA.comment) {
			const postFormData = {
				postId: this.props.postID,
				...formDATA,
			};
			this.props.createComment(postFormData);
		} else {
			alert('no-no its empty');
		}
	};
	render() {
		return (
			<CreateCommentView>
				<Form submitAction={this.submit} submitBtnText="Send">
					<Textarea
						key="body"
						type="text"
						appearing="input-block_gray-bg"
						placeholder="Comment..."
						name="comment"
						rows={4}
					/>
				</Form>
			</CreateCommentView>
		);
	}
}
