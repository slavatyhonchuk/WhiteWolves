import React from 'react';
import { Spin, Icon } from 'antd';
import './preloader.css';

const Preloader = ({ size = 24, ...props }) => {
	const antIcon = <Icon type="loading" style={{ fontSize: size }} spin />;
	return (
		<section className="Preloader">
			<Spin indicator={antIcon} />
		</section>
	);
};

export default Preloader;
