import React, { Component } from 'react';
import './form.css';

export default class Form extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}
	onKeyDown = (event) => {
		if (this.props.onEnterSubmit && event.key === 'Enter') {
			event.preventDefault();
			event.stopPropagation();
			this.props.submitAction(this.state);
		}
	};
	handleInputChange = (val, name) => {
		//  copy state
		const formData = { ...this.state };
		//  modify copied state
		formData[name] = val;
		// set modified state
		this.setState({
			...formData,
		});
	};
	render() {
		const { children } = this.props;
		const childrenWithProps = React.Children.map(children, (child) =>
			React.cloneElement(child, {
				onChange: this.handleInputChange,
				onKeyDown: this.onKeyDown,
			})
		);
		return (
			<form className="main-form">
				<div className="main-form__fields">{childrenWithProps}</div>
				<button
					type="button"
					className="main-form__submit btn btn_submit"
					onClick={() => this.props.submitAction(this.state)}
				>
					{this.props.submitBtnText}
				</button>
			</form>
		);
	}
}
