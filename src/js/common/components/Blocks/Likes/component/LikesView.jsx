import React, { Fragment } from 'react';
import './../likes-block.css';

export const LikesView = (props) => (
	<div
		className={props.liked ? 'LikesView liked' : 'LikesView'}
		onClick={props.action}
	>
		<div className="LikesView__icon">{props.liked ? '♥' : '♡'}</div>
		<div className="LikesView__count">{props.count}</div>
	</div>
);
export default LikesView;
