import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { actions as likeActions } from '../../../../../redux/modules/posts';
import { LikesView } from '../component/LikesView';

const mapDispatchToProps = {
	...likeActions,
};

@connect(
	null,
	mapDispatchToProps
)
export class Likes extends PureComponent {
	toggleLike = () => {
		const postIdObj = { postId: this.props.parentID };
		const postId = this.props.parentID;
		if (!this.props.likes.liked) {
			const like = true;
			this.props.togglePostIsLiked({ postIdObj, like });
		} else {
			const like = false;
			this.props.togglePostIsLiked({ postId, like });
		}
	};
	render() {
		return (
			<div className="Likes">
				<LikesView
					count={this.props.likes.count}
					liked={this.props.likes.liked}
					action={this.toggleLike}
				/>
			</div>
		);
	}
}
export default Likes;
