import React from 'react';
import { List, Avatar } from 'antd';

const UsersList = ({ data }) => {
	return (
		<List
			itemLayout="horizontal"
			dataSource={data}
			renderItem={(item) => (
				<List.Item>
					<List.Item.Meta
						avatar={<Avatar src={item.avatar} />}
						title={
							<a href={`/#/user/${item.userId}`}>{item.userId}</a>
						}
					/>
				</List.Item>
			)}
		/>
	);
};
export default UsersList;
