import React from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import './button.css';

const Button = (props) => (
	<div
		onClick={props.linkTo ? () => props.push(props.linkTo) : props.action}
		className={props.appearing ? `${props.appearing} btn` : 'btn '}
	>
		<span> {props.text}</span>
	</div>
);

const mapDispatchToProps = {
	push,
};

export default connect(
	null,
	mapDispatchToProps
)(Button);
