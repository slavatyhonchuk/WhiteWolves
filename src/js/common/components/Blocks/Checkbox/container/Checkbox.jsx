import React, { PureComponent } from 'react';
import { CheckboxView } from '../component/CheckboxView';

export default class Checkbox extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			isActive: this.props.isActive || false,
		};
	}

	toggle = () => {
		const status = !this.state.isActive;
		this.setState({
			isActive: status,
		});
		this.props.returnStatus(status, this.props.name);
	};

	render() {
		const { isActive } = this.state;
		return (
			<CheckboxView
				action={this.toggle}
				isActive={isActive ? 'active' : 'inactive'}
			/>
		);
	}
}
