import React from 'react';
import '../checkbox.css';

export const CheckboxView = ({ isActive, action }) => (
	<div className={`checkbox ${isActive}`} onClick={action}>
		<div className="checkbox_check">✔</div>
	</div>
);
