import React, { PureComponent } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { message } from 'antd';
import { setUserToken, setUserInfo } from '../../../../redux/modules/user';
import { Input } from '../../Blocks/Input';

import './Login.css';

const mapStateToProps = (state) => ({
	userToken: state.user.userToken,
});

const mapDispatchToProps = {
	setUserToken,
	setUserInfo,
	push,
};
@connect(
	mapStateToProps,
	mapDispatchToProps
)
class Login extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			login: {
				grant_type: 'password',
			},
			errors: [],
		};
		// eslint-disable-next-line
		axios.defaults.headers.common['Authorization'] =
			'Basic YnJvd3Nlcjpicm93c2Vy';

		this.toUrlEncoded = (obj) =>
			Object.keys(obj)
				.map(
					(k) =>
						// eslint-disable-next-line
						encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])
				)
				.join('&');
	}

	handleInputChange = (val, name) => {
		//  copy state
		const loginForm = { ...this.state.login };
		//  modify copied state
		loginForm[name] = val;
		// set modified state
		this.setState({
			login: loginForm,
		});
	};

	handleSubmit = () => {
		const api = 'http://88.99.217.199/uaa/oauth/token';
		const dataObj = { ...this.state.login };
		const load = message.loading('Loading');
		axios({
			method: 'post',
			url: api,
			data: this.toUrlEncoded(dataObj),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((res) => {
				const userTokens = res.data;
				if (res.status === 200) {
					this.props.setUserInfo({ userToken: userTokens });
				}
				return userTokens;
			})
			.then((res) => {
				const apiGET = 'http://88.99.217.199/uaa/users/current';
				axios({
					method: 'get',
					url: apiGET,
					headers: {
						Authorization: `Bearer ${res.access_token}`,
					},
				})
					.then((result) => {
						const info = result.data.principal;
						this.props.setUserInfo({ userInfo: info });
						const bearerToken = res.access_token;
						if (bearerToken) {
							axios.defaults.headers.common.Authorization = `Bearer ${bearerToken}`;
						}
						axios.defaults.baseURL = 'http://88.99.217.199';
						message.success('Success!');
						load();
						this.props.push('/account/edit');
					})
					.catch((e) => {
						console.log(e);
						message.error('Error! Try again.');
						load();
					});
			})
			.catch((e) => {
				console.log(e);
				message.error('Error! Try again.');
				load();
			});
	};

	render() {
		localStorage.clear();
		return (
			<div className="login content-card">
				<h1>Login</h1>
				<Input
					heading="Username"
					type="text"
					appearing="input-block_gray-bg"
					name="username"
					onChange={this.handleInputChange}
				/>
				<Input
					heading="Password"
					type="password"
					appearing="input-block_gray-bg"
					name="password"
					onChange={this.handleInputChange}
				/>
				<button className="btn btn_submit" onClick={this.handleSubmit}>
					SUBMIT
				</button>
				<div className="errors">
					{this.state.errors.map((item) => (
						<p className="error" key={item.defaultMessage}>
							{item.defaultMessage}
						</p>
					))}
				</div>
			</div>
		);
	}
}

export default Login;
