import React, { PureComponent } from 'react';
import axios from 'axios';
import { message } from 'antd';
import 'antd/dist/antd.css';
import { email, phone } from '../../../../utility/validation';
import { Input } from '../../Blocks/Input';
import { Select } from '../../Blocks/Select';

import './Registration.css';

class Registration extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			reg: {},
			phoneValid: true,
			emailValid: true,
			errors: [],
		};
		this.selectOptions = [
			{
				val: 'MALE',
			},
			{
				val: 'FEMALE',
			},
		];
	}

	validateEmail = (val) => {
		const status = email(val);
		if (status === null) {
			this.setState({
				emailValid: true,
			});
		} else {
			this.setState({
				emailValid: false,
			});
		}
	};
	validatePhone = (val) => {
		const status = phone(val);
		if (status === null) {
			this.setState({
				phoneValid: true,
			});
		} else {
			this.setState({
				phoneValid: false,
			});
		}
	};

	handleInputChange = (val, name) => {
		console.log(`val = ${val}, name = ${name}`);
		//  copy state
		const regForm = { ...this.state.reg };
		//  modify copied state
		regForm[name] = val;
		// set modified state
		this.setState({
			reg: regForm,
		});
	};

	handleSubmit = () => {
		const load = message.loading('Loading');

		const api = 'http://88.99.217.199/uaa/registration/request';
		axios
			.post(api, this.state.reg)
			.then((res) => {
				console.log(res);
				message.success('Success!');
				load();
			})
			.catch((e) => {
				console.log(e);
				const [...errorsArr] = e.response.data.errors;
				this.setState({
					errors: errorsArr,
				});
				message.error('Error! Fill required fields.');
				load();

				setTimeout(() => {
					this.setState({
						errors: [],
					});
				}, 7000);
			});
	};

	render() {
		localStorage.clear();
		return (
			<div className="registration content-card">
				<h1>Registration</h1>
				<Input
					heading="First Name"
					type="text"
					appearing="input-block_gray-bg"
					placeholder="Nicola"
					name="firstName"
					onChange={this.handleInputChange}
				/>
				<Input
					heading="Last Name"
					type="text"
					appearing="input-block_gray-bg"
					placeholder="Tesla"
					name="lastName"
					onChange={this.handleInputChange}
				/>
				<Input
					heading="Nickname"
					type="text"
					appearing="input-block_gray-bg"
					placeholder="tesla"
					name="nickName"
					onChange={this.handleInputChange}
				/>

				<Input
					heading="City"
					type="text"
					appearing="input-block_gray-bg"
					placeholder="Belgrad"
					name="city"
					onChange={this.handleInputChange}
				/>
				<Input
					heading="Country"
					type="text"
					appearing="input-block_gray-bg"
					placeholder="Serbia"
					name="country"
					onChange={this.handleInputChange}
				/>
				<Input
					heading="Password"
					type="password"
					appearing="input-block_gray-bg"
					placeholder="Password"
					name="password"
					onChange={this.handleInputChange}
				/>
				<Input
					heading="Email"
					type="email"
					placeholder="nicola.tesla@gmail.com"
					name="email"
					appearing={
						this.state.emailValid
							? 'input-block_gray-bg'
							: 'input-block_gray-bg input-block_not-valid'
					}
					onChange={(val, name) => {
						this.handleInputChange(val, name);
						this.validateEmail(val, name);
					}}
				/>

				<Input
					heading="Phone Number"
					type="tel"
					placeholder="+380936563214"
					name="phoneNumber"
					appearing={
						this.state.phoneValid
							? 'input-block_gray-bg'
							: 'input-block_gray-bg input-block_not-valid'
					}
					onChange={(value, name) => {
						this.handleInputChange(value, name);
						this.validatePhone(value, name);
					}}
				/>
				<h5
					style={{ marginTop: '0.75rem', color: 'hsl(0, 0%, 61%)' }}
					className="input-block__heading"
				>
					Gender
				</h5>
				<Select
					selected="Gender"
					appereance=""
					options={this.selectOptions}
					passVal={(val) => {
						this.handleInputChange(val, 'gender');
					}}
				/>
				<Input
					heading="Birthday"
					type="date"
					appearing="input-block_gray-bg"
					placeholder="1956-07-10"
					name="birthday"
					onChange={this.handleInputChange}
				/>
				<button className="btn btn_submit" onClick={this.handleSubmit}>
					SUBMIT
				</button>
				<div className="errors">
					{this.state.errors.map((item) => (
						<p className="error" key={item.defaultMessage}>
							{item.defaultMessage}
						</p>
					))}
				</div>
			</div>
		);
	}
}

export default Registration;
