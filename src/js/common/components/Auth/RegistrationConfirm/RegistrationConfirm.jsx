import React, { PureComponent } from 'react';
import axios from 'axios';
import { Spin } from 'antd';
import './RegistrationConfirm.css';

class RegistrationConfirm extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			success: false,
			error: false,
			loading: true,
		};
	}

	componentDidMount() {
		const API_LINK = 'http://88.99.217.199/uaa/registration/verify';
		const tokenURL = this.props.location.search;
		const TOKEN = tokenURL.slice(7);
		axios
			.post(API_LINK, { token: TOKEN })
			.then((res) => {
				console.log(res);
				if (res.status === 200) {
					this.setState({
						success: true,
						error: false,
						loading: false,
					});
				}
			})
			.catch((e) => {
				console.log(e);
				this.setState({
					success: false,
					error: true,
					loading: false,
				});
			});
	}
	render() {
		localStorage.clear();
		return (
			<div className="RegistrationConfirm content-card">
				{this.state.loading ? (
					<Spin />
				) : this.state.error ? (
					<p>ERROR</p>
				) : this.state.success ? (
					<h1 style={{ textAlign: 'center' }}>EMAIL CONFIRMED</h1>
				) : (
					false
				)}
			</div>
		);
	}
}

export default RegistrationConfirm;
