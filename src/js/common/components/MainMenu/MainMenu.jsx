import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import './MainMenu.css';

const mapStateToProps = (state) => ({
	userToken: state.user.userInfo.id,
});
@connect(mapStateToProps)
class MainMenu extends PureComponent {
	render() {
		return (
			<aside className="globalMenu">
				<Link to={`/user/${this.props.userToken}`}>Profile</Link>
				<Link to="/feed">Feed</Link>
				<Link to="/messenger">Messenger</Link>
				<a href="/" style={{ pointerEvents: 'none' }}>
					_________________
				</a>
				<Link to="/auth/login">Login</Link>
				<Link to="/auth/registration">Registration</Link>
				<Link to="/account/requestChangePassword">Change Password</Link>
			</aside>
		);
	}
}

export default MainMenu;
