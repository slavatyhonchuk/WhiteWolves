import React, { PureComponent } from 'react';
import axios from 'axios';
import { message } from 'antd';
import { connect } from 'react-redux';
import { Select } from '../../Blocks/Select';

import './ConfidentialSettings.css';
import Checkbox from '../../Blocks/Checkbox/container/Checkbox';

const mapStateToProps = (state) => ({
	userId: state.user.userInfo.id,
	userToken: state.user.userToken.access_token,
});

@connect(mapStateToProps)
class ConfidentialSettings extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			form: {},
			errors: [],
			loading: true,
		};

		this.nameVisiblitySelect = [
			{
				val: 'Only Nickname',
			},
			{
				val: 'Only Full Name',
			},
			{
				val: 'Full Name & Nickname',
			},
		];
		this.allowCommentsSelect = [
			{
				val: 'ALL',
			},
			{
				val: 'FOLLOWERS',
			},
			{
				val: 'NOBODY',
			},
		];
		this.allowMessagesSelect = [
			{
				val: 'ALL',
			},
			{
				val: 'FOLLOWERS',
			},
			{
				val: 'NOBODY',
			},
		];

		axios({
			method: 'get',
			url: `http://88.99.217.199/profile-service/confidential?user_id=${
				this.props.userId
			}`,
			headers: {
				Authorization: `Bearer ${this.props.userToken}`,
			},
		}).then((res) => {
			const apiData = res.data;
			const nameVis = () => {
				if (
					apiData.showNickName === true &&
					apiData.showFullName == false
				) {
					return 'Only Nickname';
				} else if (
					apiData.showNickName === false &&
					apiData.showFullName === true
				) {
					return 'Only Full Name';
				} else if (
					apiData.showNickName === true &&
					apiData.showFullName === true
				) {
					return 'Full Name & Nickname';
				}
				return 'Choose';
			};
			this.setState({
				form: {
					userId: this.props.userId,
					allowComments: apiData.allowComments,
					allowMessages: apiData.allowMessages,
					showBirthday: apiData.showBirthday,
					showFullName: apiData.showFullName,
					showNickName: apiData.showNickName,
					showResidence: apiData.showResidence,
				},
				nameVisiblity: nameVis(),
				loading: false,
			});
		});
	}

	handleInputChange = (val, name) => {
		//  copy state
		const formData = { ...this.state.form };

		switch (val) {
			case 'Only Nickname':
				formData.showNickName = true;
				formData.showFullName = false;
				break;
			case 'Only Full Name':
				formData.showNickName = false;
				formData.showFullName = true;
				break;
			case 'Full Name & Nickname':
				formData.showNickName = true;
				formData.showFullName = true;
				break;
			default:
				formData[name] = val;
				break;
		}

		//  modify copied state

		// set modified state
		this.setState({
			form: formData,
		});
	};

	handleSubmit = () => {
		const api = 'http://88.99.217.199/profile-service/confidential/update';
		const { ...form } = this.state.form;
		const load = message.loading('Loading!', 0);

		axios({
			method: 'post',
			url: api,
			data: form,
			headers: {
				Authorization: `Bearer ${this.props.userToken}`,
			},
		})
			.then((res) => {
				console.log(res);
				message.success('Success!');
				load();
			})
			.catch((e) => {
				const [...errorsArr] = e.response.data.errors;
				this.setState({
					errors: errorsArr,
				});
				message.error('Error! Try again.');
				load();
				setTimeout(() => {
					this.setState({
						errors: [],
					});
				}, 7000);
			});
	};

	render() {
		return this.state.loading ? (
			<h1>Loading</h1>
		) : (
			<div className="edit content-card">
				<h1>Confidential Settings</h1>
				<div className="settings-block">
					<div className="settings-block__row">
						<div className="select_block">
							<h5 className="input-block__heading">
								Name Visiblity
							</h5>
							<Select
								selected={
									this.state.nameVisiblity
										? this.state.nameVisiblity
										: 'Select'
								}
								key="nameVisiblity"
								appereance=""
								options={this.nameVisiblitySelect}
								passVal={(val) => {
									this.handleInputChange(
										val,
										'nameVisiblity'
									);
								}}
							/>
						</div>
						<div className="select_block">
							<h5 className="input-block__heading">
								Allow Comments
							</h5>
							<Select
								selected={
									this.state.form.allowComments
										? this.state.form.allowComments
										: 'Select'
								}
								key="allowComments"
								appereance=""
								options={this.allowCommentsSelect}
								passVal={(val) => {
									this.handleInputChange(
										val,
										'allowComments'
									);
								}}
							/>
						</div>
						<div className="select_block">
							<h5 className="input-block__heading">
								Allow Messages
							</h5>
							<Select
								selected={
									this.state.form.allowMessages
										? this.state.form.allowMessages
										: 'Select'
								}
								key="allowComments"
								appereance=""
								options={this.allowMessagesSelect}
								passVal={(val) => {
									this.handleInputChange(
										val,
										'allowMessages'
									);
								}}
							/>
						</div>
					</div>
					<div className="settings-block__row">
						<div className="checkbox-block">
							<h5 className="input-block__heading">
								Show Birthday
							</h5>
							<Checkbox
								name="showBirthday"
								isActive={this.state.form.showBirthday}
								returnStatus={(val, name) =>
									this.handleInputChange(val, name)
								}
							/>
						</div>
						<div className="checkbox-block">
							<h5 className="input-block__heading">
								showResidence
							</h5>
							<Checkbox
								name="showResidence"
								isActive={this.state.form.showResidence}
								returnStatus={(val, name) =>
									this.handleInputChange(val, name)
								}
							/>
						</div>
					</div>
				</div>
				<button className="btn btn_submit" onClick={this.handleSubmit}>
					SUBMIT
				</button>
				<div className="errors">
					{this.state.errors.map((item) => (
						<p className="error" key={item.defaultMessage}>
							{item.defaultMessage}
						</p>
					))}
				</div>
			</div>
		);
	}
}

export default ConfidentialSettings;
