import React, { PureComponent } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Input } from '../../Blocks/Input';

const mapStateToProps = (state) => ({
	userEmail: state.user.userInfo.email,
});

const mapDispatchToProps = {
	push,
};

@connect(
	mapStateToProps,
	mapDispatchToProps
)
class RequestChangePass extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			userEmail: '',
		};
	}
	handleInputChange = (val, name) => {
		//  copy state
		const formData = { ...this.state };
		//  modify copied state
		formData[name] = val;
		// set modified state
		console.log(formData)
		this.setState({
			...formData,
		});
	};
	handleSubmit = () => {
		const api =
			'http://88.99.217.199/uaa/registration/requestPasswordChange';
		const dataObj = {
			email: this.state.userEmail,
		};
		axios
			.post(api, dataObj)
			.then((res) => {
				if (res.status === 200) {
					alert('success');
				}
				console.log(res);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	render() {
		return (
			<div className="change-pass content-card">
				<Input
					key="userEmail"
					value={this.state.userEmail}
					heading="Email"
					type="text"
					appearing="input-block_gray-bg"
					placeholder="Email"
					name="userEmail"
					onChange={this.handleInputChange}
				/>
				<button onClick={this.handleSubmit} className="btn btn_submit">
					Request Change
				</button>
			</div>
		);
	}
}

export default RequestChangePass;
