import React, { Component } from 'react';
import axios, { post } from 'axios';
import Button from '../../Blocks/Button/Button';
import './LoadAvatar.css';
import { UPLOAD_AVATAR, DELETE_AVATAR } from '../../../API/API_URLS';

class LoadAvatar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			file:
				this.props.userAvatar ||
				'https://s3.amazonaws.com/cdn.33voices.com/defaults/avatar.png',
			uploadedNew: false,
		};
	}

	handleChange = (event) => {
		const file = event.target.files[0];
		this.setState({
			file: URL.createObjectURL(file),
			imageFile: file,
			uploadedNew: true,
		});
	};
	submit = () => {
		const formData = new FormData();
		formData.append('file', this.state.imageFile);
		post(UPLOAD_AVATAR, formData)
			.then((res) => {
				const imageUrl = `${res.data}?ver=${Date.now()}`;
				console.log(imageUrl);
				this.props.liftAvatar(imageUrl);
			})
			.catch((err) => {
				console.log(err);
			});
	};
	delete = () => {
		axios
			.delete(DELETE_AVATAR)
			.then((res) => {
				console.log(res);
				this.setState({
					file:
						'https://s3.amazonaws.com/cdn.33voices.com/defaults/avatar.png',
				});
				this.props.liftAvatar('');
			})
			.catch((err) => {
				console.log(err);
			});
	};
	render() {
		const showBtn = this.props.userAvatar !== '' || this.state.imageFile;
		console.log(showBtn);
		return (
			<div className="image-loader content-card">
				<input
					className="image-loader__input"
					type="file"
					onChange={this.handleChange}
					name={this.props.name}
				/>
				<img
					src={this.state.file}
					alt=""
					className="account-card__avatar image-loader__img"
				/>
				{showBtn ? (
					<Button
						appearing="btn_submit"
						action={
							this.state.uploadedNew ? this.submit : this.delete
						}
						text={this.state.uploadedNew ? 'Upload' : 'Delete'}
					/>
				) : null}
			</div>
		);
	}
}

export default LoadAvatar;
