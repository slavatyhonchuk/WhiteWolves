import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { message } from 'antd';
import 'antd/dist/antd.css';
import { phone } from '../../../../utility/validation';
import { Input } from '../../Blocks/Input';
import Textarea from '../../Blocks/Textarea/Textarea';
import { Select } from '../../Blocks/Select';
import LoadAvatar from '../LoadAvatar/LoadAvatar';
import ConfidentialSettings from '../ConfidentialSettings/ConfidentialSettings';
import './Edit.css';

const mapStateToProps = (state) => ({
	userId: state.user.userInfo.id,
	userToken: state.user.userToken.access_token,
});
@connect(mapStateToProps)
class Edit extends Component {
	constructor(props) {
		super(props);
		this.state = {
			form: {},
			phoneValid: true,
			errors: [],
			loading: true,
		};
		this.selectOptions = [
			{
				val: 'MALE',
			},
			{
				val: 'FEMALE',
			},
		];

		axios({
			method: 'get',
			url: `http://88.99.217.199/profile-service/profile?user_id=${
				this.props.userId
			}`,
			headers: {
				Authorization: `Bearer ${this.props.userToken}`,
			},
		}).then((res) => {
			const user = res.data;
			console.log(user);
			this.setState({
				form: {
					userId: this.props.userId,
					firstName: user.firstName,
					lastName: user.lastName,
					nickName: user.nickName,
					city: user.city,
					country: user.country,
					phoneNumber: user.phoneNumber,
					gender: user.gender,
					birthday: user.birthday,
					avatar: user.avatar,
					about: user.about,
				},
				loading: false,
			});
		});
	}
	validatePhone = (val) => {
		const status = phone(val);
		if (status === null) {
			this.setState({
				phoneValid: true,
			});
		} else {
			this.setState({
				phoneValid: false,
			});
		}
	};
	handleInputChange = (val, name) => {
		//  copy state
		const formData = { ...this.state.form };
		//  modify copied state
		formData[name] = val;
		// set modified state
		this.setState({
			form: formData,
		});
	};

	handleSubmit = () => {
		const api = 'http://88.99.217.199/profile-service/profile/update';
		const { ...form } = this.state.form;
		const load = message.loading('Loading!');

		axios({
			method: 'post',
			url: api,
			data: form,
			headers: {
				Authorization: `Bearer ${this.props.userToken}`,
			},
		})
			.then((res) => {
				message.success('Success!');
				load();
				console.log(res);
			})
			.catch((e) => {
				message.error('Error! Try again.');
				load();

				const [...errorsArr] = e.response.data.errors;
				this.setState({
					errors: errorsArr,
				});
				setTimeout(() => {
					this.setState({
						errors: [],
					});
				}, 7000);
			});
	};
	receiveAvatar = (link) => {
		this.setState({
			form: {
				...this.state.form,
				avatar: link,
			},
		});
		this.handleSubmit();
	};
	render() {
		return this.state.loading ? (
			<h1>Loading</h1>
		) : (
			<div className="edit">
				<h1>Edit</h1>
				<section className="EditProfile-wrapper">
					<div className="LoadAvatar-wrapper">
						<LoadAvatar
							userAvatar={this.state.form.avatar}
							liftAvatar={this.receiveAvatar}
						/>
					</div>
					<div className="settings-wrap">
						<div className="EditProfile-fields-wrapper content-card">
							<Input
								key="firstName"
								value={this.state.form.firstName}
								heading="First Name"
								type="text"
								appearing="input-block_gray-bg"
								placeholder="Nicola"
								name="firstName"
								onChange={this.handleInputChange}
							/>
							<Input
								heading="Last Name"
								type="text"
								appearing="input-block_gray-bg"
								placeholder="Tesla"
								name="lastName"
								key="lastName"
								onChange={this.handleInputChange}
								value={this.state.form.lastName}
							/>
							<Input
								heading="Nickname"
								type="text"
								appearing="input-block_gray-bg"
								placeholder="tesla"
								name="nickName"
								key="nickName"
								onChange={this.handleInputChange}
								value={this.state.form.nickName}
							/>

							<Input
								heading="City"
								type="text"
								appearing="input-block_gray-bg"
								placeholder="Belgrad"
								name="city"
								key="city"
								onChange={this.handleInputChange}
								value={this.state.form.city}
							/>
							<Input
								heading="Country"
								type="text"
								appearing="input-block_gray-bg"
								placeholder="Serbia"
								name="country"
								key="country"
								onChange={this.handleInputChange}
								value={this.state.form.country}
							/>

							<Input
								heading="Phone Number"
								type="tel"
								placeholder="+380936563214"
								name="phoneNumber"
								key="phoneNumber"
								appearing={
									this.state.phoneValid
										? 'input-block_gray-bg'
										: 'input-block_gray-bg input-block_not-valid'
								}
								onChange={(value, name) => {
									this.handleInputChange(value, name);
									this.validatePhone(value, name);
								}}
								value={this.state.form.phoneNumber}
							/>
							<h5
								style={{
									marginTop: '0.75rem',
								}}
								className="input-block__heading"
							>
								Gender
							</h5>
							<Select
								selected={
									this.state.form.gender
										? this.state.form.gender
										: 'Gender'
								}
								key="gender"
								appereance=""
								options={this.selectOptions}
								passVal={(val) => {
									this.handleInputChange(val, 'gender');
								}}
							/>
							<Input
								heading="Birthday"
								key="birthday"
								type="date"
								appearing="input-block_gray-bg"
								placeholder="1956-07-10"
								name="birthday"
								onChange={this.handleInputChange}
								value={this.state.form.birthday}
							/>
							<Textarea
								key="about"
								type="text"
								appearing="input-block_gray-bg"
								placeholder="About..."
								name="about"
								rows={4}
								onChange={this.handleInputChange}
								value={this.state.form.about}
							/>
							<button
								className="btn btn_submit"
								onClick={this.handleSubmit}
							>
								SUBMIT
							</button>
						</div>
						<section>
							<ConfidentialSettings />
						</section>
					</div>
					<div className="errors">
						{this.state.errors.map((item) => (
							<p className="error" key={item.defaultMessage}>
								{item.defaultMessage}
							</p>
						))}
					</div>
				</section>
			</div>
		);
	}
}

export default Edit;
