import React, { PureComponent } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { Input } from '../../Blocks/Input';
import './ChangePass.css';

const mapStateToProps = (state) => ({
	userEmail: state.user.userInfo.email,
});

const mapDispatchToProps = {
	push,
};

@connect(
	mapStateToProps,
	mapDispatchToProps
)
class ChangePass extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			form: {},
			errors: [],
		};
		const tokenURL = this.props.location.search;
		this.TOKEN = tokenURL.slice(7);
	}
	handleInputChange = (val, name) => {
		//  copy state
		const formData = { ...this.state.form };
		//  modify copied state
		formData[name] = val;
		// set modified state
		this.setState({
			form: formData,
		});
	};

	handleSubmit = () => {
		const api = 'http://88.99.217.199/uaa/registration/changePassword';
		const dataObj = {
			email: this.props.userEmail,
			newPassword: this.state.form.newPassword,
			token: this.TOKEN,
		};
		console.log(dataObj);
		axios
			.post(api, dataObj)
			.then((res) => {
				if (res.status === 200) {
					alert('success');
				}
			})
			.catch((e) => {
				const [...errorsArr] = e.response.data.errors;
				this.setState({
					errors: errorsArr,
				});
				setTimeout(() => {
					this.setState({
						errors: [],
					});
				}, 7000);
			});
	};
	render() {
		return (
			<div className="change-pass content-card">
				<h1>New Pass</h1>
				<Input
					type="password"
					appearing="input-block_gray-bg"
					placeholder="New Password"
					name="newPassword"
					onChange={this.handleInputChange}
				/>
				<div className="hint">a digit must occur at least once</div>
				<div className="hint">
					a lower case letter must occur at least once
				</div>
				<div className="hint">
					an upper case letter must occur at least once
				</div>
				<div className="hint">
					no whitespace allowed in the entire string
				</div>
				<div className="hint">at least 6 characters</div>

				<button onClick={this.handleSubmit} className="btn btn_submit">
					SUBMIT
				</button>
				<div className="errors">
					{this.state.errors.map((item) => (
						<p className="error" key={item.defaultMessage}>
							{item.defaultMessage}
						</p>
					))}
				</div>
			</div>
		);
	}
}

export default ChangePass;
