import React from 'react';
import Chat from '../messenger-components/Chat/container/Chat';
import ContactsList from '../messenger-components/ContactsList/container/ContactsList';
import '../messenger.css';

const MessengerView = (props) => (
	<div className="MessengerView content-card">
		<main className="MessengerView__main">
			<Chat data={props.chatData} />
		</main>
		<aside className="MessengerView__aside">
			<ContactsList contacts={props.contacts} />
		</aside>
	</div>
);
export default MessengerView;
