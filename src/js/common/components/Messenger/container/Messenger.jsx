import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessengerView from '../component/MessengerView';
import { actions as messangerActions } from '../../../../redux/modules/messanger';

const mapStateToProps = (state) => ({
	...state.messanger,
	userToken: state.user.userToken.access_token,
});
const mapDispatchToProps = {
	...messangerActions,
};
@connect(
	mapStateToProps,
	mapDispatchToProps
)
export default class Messenger extends Component {
	constructor(props) {
		super(props);
		this.props.getContacts();
	}
	render() {
		return (
			<MessengerView
				contacts={this.props.contactsList}
				chatData={this.props.currentChat}
			/>
		);
	}
}
