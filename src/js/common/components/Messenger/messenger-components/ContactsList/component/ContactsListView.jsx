import React from 'react';
import '../contact-list.css';

const ContactsListView = (props) => (
	<div className="ContactsListView">
		<div>{props.children}</div>
	</div>
);

export default ContactsListView;
