import React from 'react';
import ContactsListView from '../component/ContactsListView';
import ContactItem from '../../ContactItem/container/ContactItem';

const ContactsList = (props) => {
	return (
		<div className="ContactsList">
			<ContactsListView>
				{props.contacts
					? props.contacts.map((item) => (
							<ContactItem key={item.userId} {...item} />
					  ))
					: null}
			</ContactsListView>
		</div>
	);
};

export default ContactsList;
