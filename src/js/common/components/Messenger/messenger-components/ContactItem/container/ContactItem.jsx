import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions as messangerActions } from '../../../../../../redux/modules/messanger';
import ContactItemView from '../component/ContactItemView';

const mapStateToProps = (state) => ({
	userToken: state.user.userToken.access_token,
	currentUserId: state.user.userInfo.id,
	currentChatUser: state.messanger.currentChat.recipient.recipientId,
});
const mapDispatchToProps = {
	...messangerActions,
};
@connect(
	mapStateToProps,
	mapDispatchToProps
)
export default class ContactItem extends Component {
	connect = (id) => {
		this.props.closeSocket();
		const recipient = {
			name: `${this.props.firstName} ${this.props.lastName} ${
				this.props.nickName
			}`,
			recipientId: this.props.userId,
			avatar: this.props.avatar,
		};
		this.props.getMessages(id, recipient);
		const WS_PROT = `http://88.99.217.199:5040/message/?access_token=${
			this.props.userToken
		}`;
		this.props.initChatWS({
			WS_PROT,
			userToken: this.props.userToken,
			currentUserId: this.props.currentUserId,
			recipientId: this.props.userId,
		});
	};

	render() {
		console.log();
		return (
			<ContactItemView
				isActive={this.props.currentChatUser === this.props.userId}
				action={this.connect}
				{...this.props}
			/>
		);
	}
}
