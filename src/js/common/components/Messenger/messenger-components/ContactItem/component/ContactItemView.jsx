import React from 'react';
import '../contact-item.css';

const ContactItemView = (props) => (
	<div
		className={props.isActive ? 'ContactItemView ContactItemView__active' : 'ContactItemView'}
		onClick={() => props.action(props.userId)}
	>
		<div className="ContactItemView__avatar-wrap">
			<img
				className="ContactItemView__avatar"
				src={props.avatar}
				alt="Avatar"
			/>
		</div>
		<div className="ContactItemView__user">
			<h5 className="ContactItemView__name">
				{`${props.firstName ? props.firstName : ''} ${
					props.lastName ? props.lastName : ''
				} ${props.nickName ? props.nickName : ''}`}
			</h5>
		</div>
	</div>
);

export default ContactItemView;
