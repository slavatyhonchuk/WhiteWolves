import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageView from '../component/MessageView';
import '../message.css';
import { markMessageAsReaded } from '../../../../../../redux/modules/messanger';

const mapStateToProps = (state) => ({
	userId: state.user.userInfo.id,
});
const mapDispatchToProps = {
	markMessageAsReaded,
};
@connect(
	mapStateToProps,
	mapDispatchToProps
)
export default class Message extends Component {
	componentDidMount() {
		if (this.props.recipient === this.props.userId && !this.props.read) {
			this.props.markMessageAsReaded(this.props.id);
		}
	}
	render() {
		return (
			<article
				className={
					this.props.sender === this.props.userId
						? 'Message Message__my'
						: 'Message'
				}
			>
				<MessageView
					{...this.props}
					isMyMessage={this.props.sender === this.props.userId}
				/>
			</article>
		);
	}
}
