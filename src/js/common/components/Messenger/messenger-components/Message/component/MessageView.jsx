import React from 'react';

const MessageView = (props) => (
	<div
		className={
			props.isMyMessage
				? 'MessageView MessageView__my'
				: 'MessageView MessageView__rec'
		}
	>
		{props.isMyMessage ? null : (
			<div className="MessageView__recipient">
				<div className="MessageView__recipient-name">
					{props.recipientName}
				</div>
				<img
					className="MessageView__recipient-avatar"
					src={props.recipientAvatar}
					alt=""
				/>
			</div>
		)}
		<div className="MessageView__body">{props.body}</div>
		<footer className="MessageView__footer">
			{props.isMyMessage ? (
				<div
					className={
						props.read
							? 'MessageView__read MessageView__read_readed'
							: 'MessageView__read'
					}
				>
					<span className="MessageView__read-arrow MessageView__read-arrow-hidden">
						<svg
							aria-hidden="true"
							data-prefix="fas"
							data-icon="check"
							role="img"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 512 512"
						>
							<path
								fill="currentColor"
								d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"
							/>
						</svg>
					</span>
					<span className="MessageView__read-arrow">
						<svg
							aria-hidden="true"
							data-prefix="fas"
							data-icon="check"
							role="img"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 512 512"
						>
							<path
								fill="currentColor"
								d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"
							/>
						</svg>
					</span>
				</div>
			) : null}
			<div className="MessageView__posted">{props.posted}</div>
		</footer>
	</div>
);

export default MessageView;
