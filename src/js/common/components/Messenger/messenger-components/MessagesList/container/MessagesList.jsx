import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessagesListView from '../component/MessagesListView';
import Message from '../../Message/container/Message';
import { actions } from '../../../../../../redux/modules/messanger';

const mapDispatchToProps = {
	...actions,
};
@connect(
	null,
	mapDispatchToProps
)
export default class MessagesList extends Component {
	componentDidUpdate() {
		this.scrollToBottom();
	}
	componentWillUnmount() {
		this.props.closeSocket();
	}
	scrollToBottom = () => {
		const objDiv = document.querySelector('.MessagesList');
		objDiv.scrollTop = objDiv.scrollHeight;
	};
	render() {
		return (
			<section className="MessagesList">
				<MessagesListView>
					{this.props.messages.messagesArr
						? this.props.messages.messagesArr.map((item) => (
								<Message
									key={item.id + item.posted}
									id={item.id}
									posted={item.posted}
									read={item.read}
									recipient={item.recipient}
									recipientAvatar={
										this.props.messages.recipient.avatar
									}
									recipientName={
										this.props.messages.recipient.name
									}
									sender={item.sender}
									body={item.body}
								/>
						  ))
						: null}
				</MessagesListView>
			</section>
		);
	}
}
