import React, { Fragment } from 'react';
import '../messages-list.css';

const MessagesListView = (props) => <Fragment>{props.children}</Fragment>;

export default MessagesListView;
