import React, { Component } from 'react';
import ChatView from '../component/ChatView';
import MessagesList from '../../MessagesList/container/MessagesList';
import MessagesInput from '../../MessageInput/container/MessageInput';

export default class Chat extends Component {
	constructor(props) {
		super(props);
		console.log(this.props.data);
	}
	render() {
		return (
			<section className="Chat">
				<ChatView>
					<MessagesList messages={this.props.data} />
					<MessagesInput />
				</ChatView>
			</section>
		);
	}
}
