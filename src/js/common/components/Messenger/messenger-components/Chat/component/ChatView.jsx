import React from 'react';

const ChatView = (props) => (
	<div className="ChatView">
		<div>{props.children}</div>
	</div>
);

export default ChatView;
