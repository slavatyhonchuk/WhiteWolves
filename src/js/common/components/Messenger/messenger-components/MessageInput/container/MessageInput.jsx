import React, { Component } from 'react';
import { Form, Input, Button } from 'antd';
import { connect } from 'react-redux';
// import Form from '../../../../Blocks/Form/Form';
// import Textarea from '../../../../Blocks/Textarea/Textarea';
import MessageInputView from '../component/MessageInputView';
import { actions as messangerActions } from '../../../../../../redux/modules/messanger';

const { TextArea } = Input;
const mapStateToProps = (state) => ({
	...state.messanger,
});
const mapDispatchToProps = {
	...messangerActions,
};
@connect(
	mapStateToProps,
	mapDispatchToProps
)
class MessageInputForm extends Component {
	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {
				this.props.sendMessageToChat(values.message);
				this.props.form.resetFields();
			}
		});
	};
	render() {
		const { getFieldDecorator } = this.props.form;
		return (
			<section className="MessageInput">
				<MessageInputView>
					<Form
						className="MessageInput__Form"
						layout="inline"
						onSubmit={this.handleSubmit}
					>
						{getFieldDecorator('message', {
							rules: [
								{
									required: true,
									message: 'Please input your message',
								},
							],
						})(<TextArea placeholder="Message" autosize />)}

						<Form.Item>
							<Button type="submit" htmlType="submit">
								Send
							</Button>
						</Form.Item>
					</Form>
				</MessageInputView>
			</section>
		);
	}
}
const MessageInput = Form.create()(MessageInputForm);
export default MessageInput;
