import React from 'react';
import '../message-input.css';

const MessageInputView = (props) => (
	<div className="MessageInputView">
		<div>{props.children}</div>
	</div>
);

export default MessageInputView;
