import React, { Fragment } from 'react';

import { ErrorBoundary } from '../../common/components/Utilities';
import { RegistrationComponent } from '../../common/components/Auth/Registration';

const RegistrationView = (props) => (
	<Fragment>
		<ErrorBoundary>
			<RegistrationComponent {...props} />
		</ErrorBoundary>
	</Fragment>
);
export default RegistrationView;
