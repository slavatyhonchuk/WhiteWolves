import React, { Fragment } from 'react';
import { ErrorBoundary } from '../../common/components/Utilities';
import { Login } from '../../common/components/Auth/Login';

const LoginView = (props) => (
	<Fragment>
		<ErrorBoundary>
			<Login {...props} />
		</ErrorBoundary>
	</Fragment>
);

export default LoginView;
