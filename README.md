## Start developing in local environment
Step 1: Clone this repo
```
git clone https://gitlab.com/profit-2018/wwolwes-front.git
cd wwolwes-front
```

Step 2: Install (in project root)

```
npm install
```

Step 3: Start (in project root)

```
npm run dev
```

And Done!

## To deploy in local environment:
Step 1: Clone this repo

```
git clone https://gitlab.com/profit-2018/wwolwes-front.git
cd wwolwes-front
```

Step 2: Install (in project root)

```
npm install
```

Step 2: Build project for deploy (in project root)

```
npm run build
```

Step 1: Make sure that you have http-server installed (in project root)

```
npm install -g http-server
```

Step 2: Go to builded project directory

```
cd dockroot
```

Step 3: Start HTTP Server (in /dockroot directory)

```
http-server -p 5779
```


## Features / Benefits

Features

* React 16
* Redux
* Saga
* ES6 / ES7
* ImmutableJS
* PreCSS ( supports SASS-like markup in your CSS )
* PostCSS ( it support CSS modules, and we recommended B.E.M style )
* Webpack 3
* Reselect
* Lazy Loading component supports
* Type Checking with Babel Type Check ( Flow syntax )
* ESLint for syntax check
* Jest and Enzyme for Unit testing

Workflow

* Development
  * Hot Module Reload during development
  * Built-in lightweight config system
  * Built-in fancy cli dashboard for reporting run time compile status
  * Built-in support for multiple device concurrent debugging
* Build / Production
  * Production bundle analyzing capability
  * CSS / HTML / JS minification / Image optimization when built
  * JS code duplication removal during built ( tree shaking capability )
* Deployment
  * Built-in git commit hook, helpful for CI/CD process
  * Built-in process to deploy files directly to S3 ( optional )
* Productivity
  * Highly configurable build and workflow system ( webpack )
  * Minimal setup time and allow you to invest into things that matters
  * Everything automatic, you just care about development, nothing else \o/ Yeah ?!

If you are interested, please read the `package.json` for all installed modules and plugins.

